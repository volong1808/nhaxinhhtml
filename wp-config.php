<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'nhaxinh' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '123456' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '&LO)K&if(h{sDi=tz(+LG:,8(f9>A19KgNCCi9o~yEqsUR!m=q,7}W#{u|DhGtTf' );
define( 'SECURE_AUTH_KEY',  'K^pBbBj;@Edxe{FLTGY57]{Eb2n>|U$X#~Xgb`k!iNA40l?|U:jFgyg0%]q0bL.m' );
define( 'LOGGED_IN_KEY',    ')xLx_MaX&&FRJsciVOfKr#B:{X6`q!^Rmr,D`d|Z9Bj/22cmK_`_`v8`|~Db9SV9' );
define( 'NONCE_KEY',        '&Ra<Q?3RwrEj|THzXd!1e|r;P/XW*?*XdKx=Cj@_z5*s`whI%*]|6]d|Fm04CnRy' );
define( 'AUTH_SALT',        '){yBwzIEqTIGC9Rihnn_oNFK-Zr0QfSgj]:4`t0<{Hx8c&r:G1g`.%/e2oypy*F!' );
define( 'SECURE_AUTH_SALT', 'kWJ?>DVT7-e)L<SmF{=if]~?sgW,::BlZhA@+&KkP!^^q|tbVb( ):SK]epLz[db' );
define( 'LOGGED_IN_SALT',   ';`..9-(=6c1#Rr&V:&Zi<A.@e>UIxi% g94db|v$i{p*<B[58i}l3`c]HxzOAX)g' );
define( 'NONCE_SALT',       '.-Ql65S-,${qbR6;IzRmlC3P$/]LizPVA62/MSUN,zJ/Jf/n^k`.CxtAVs6|t}}X' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
