var nav_header = $("#navHeader");
var sticky = nav_header.offset().top;
$(window).scroll(function(){
    console.log(sticky);
    if ($(window).scrollTop() > sticky) {
        nav_header.addClass("sticky");
    } else {
        nav_header.removeClass("sticky");
    }
});

$(document).ready(function () {
    var isMenu = false;
    $('#btn-menu').click(function () {
        if (isMenu === false) {
            isMenu = true;
        } else {
            isMenu = false;
        }
        console.log(isMenu);
        if (isMenu === true) {
            $('body').css('overflowY', 'hidden')
            $('.menu_sp').show();
        } else {
            $('body').css("overflowY", 'auto');
            $('.menu_sp').hide();
        }
    });



    /*Scroll Top */
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('#scroll-top').fadeIn();
        } else {
            $('#scroll-top').fadeOut();
        }
    });

    $('#scroll-top').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 300);
        return false;
    });

});