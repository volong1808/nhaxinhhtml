<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nhaxinh
 */

?>

<!--<section class="partner">-->
<!--    <span>Đối tác bên ngoài: </span>-->
<!--    <a href="javascript:void(0)"><span>bảo hiểm daiichi hàng đầu</span></a>|&nbsp;-->
<!--    <a href="javascript:void(0)">link Fb88: nhà cái Fb88</a>|&nbsp;-->
<!--    <a href="javascript:void(0)">NHÀ XINH CENTER</a>|&nbsp;-->
<!--    <a href="javascript:void(0)">tỷ giá ngoại tệ của sacom bank</a>|&nbsp;-->
<!--    <a href="javascript:void(0)">Cong ty nha xinh sai gon</a>|&nbsp;-->
<!--    <a href="javascript:void(0)">tỷ giá ngoại tệ techcombank mới</a>|&nbsp;-->
<!--    <a href="javascript:void(0)"><span>bảo hiểm daiichi Việt nam</span></a>|&nbsp;-->
<!--    <a href="javascript:void(0)">Tu van thiết kế nhà đẹp - thiet ke biet thu dep</a>|&nbsp;-->
<!--    <a href="javascript:void(0)">tỷ giá ngân hàng nhà nước hôm nay</a>|&nbsp;-->
<!--    <a href="javascript:void(0)">Thiet ke nha xinh| tỷ giá php vnd vietcombank mới nhất</a>|&nbsp;-->
<!--    <a href="javascript:void(0)">tra cứu tỷ giá euro vietinbank</a>|&nbsp;-->
<!--    <a href="javascript:void(0)">tỷ giá yên nhật ở bidv vn</a>|&nbsp;-->
<!--    <a href="javascript:void(0)">thông tin tỷ giá euro eximbank</a>|&nbsp;-->
<!--    <a href="javascript:void(0)">tỷ giá euro đông á bank</a>|&nbsp;-->
<!--    <a href="javascript:void(0)">ti gia usd hsbc bank</a>|&nbsp;-->
<!--    <a href="javascript:void(0)">tỷ giá ngân hàng mb yên nhật ở vn</a>|&nbsp;-->
<!--    <a href="javascript:void(0)">tỷ giá ngoại tệ shb mới</a>-->
<!--</section>-->
<footer id="footer">
    <p class="text-uppercase"><?php echo get_option('company_name'); ?></p>
    <p>
        Trụ Sở Chính: <?php echo get_option('address'); ?>
        <br>
        Văn Phòng Kiến Trúc Xây Dựng: <?php echo get_option('address_vp'); ?>
        <br>
        Hot Line: <a href="tel:<?php echo get_option('hot_line1'); ?>" target="_top" class="font-weight-bold"><?php echo get_option('hot_line1'); ?></a> - Email: <a href="mailto:<?php echo get_option('email'); ?>" target="_top"><?php echo get_option('email'); ?></a
    </p>
    <p style="color:#ff0000; ">Website đã đăng ký với bộ công thương</p>
</footer>
<div class="footer-bottom" style="background: #1E1E20; padding: 25px 0;"></div>
<div class="menu-bottom text-center">
    <?php
    wp_nav_menu(array(
        'theme_location' => 'menu-2',
        'menu_class' => 'menu2',
    )); ?>
</div>
</div>

<a id="send-message" href="tel:<?php echo get_option('hot_line1'); ?>">
    <i class="fa fa-envelope mr-3" aria-hidden="true"></i>
    <span>Tư Vấn Giám Sát - <?php echo get_option('hot_line1'); ?></span>
</a>

<a id="scroll-top">
    <i class="fa fa-angle-double-up" aria-hidden="true"></i>
</a>

<div class="fixed-social">
    <div class="item social-phone">
        <a href="tel:<?php echo get_option('hot_line1'); ?>" target="_blank">
            <img src="<?php echo get_template_directory_uri() ?>/images/icon-phone.svg" alt="icon">
        </a>
    </div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="<?php echo get_template_directory_uri() ?>/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="<?php echo get_template_directory_uri() ?>/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for this template -->
<script src="<?php echo get_template_directory_uri() ?>/js/main.js"></script>

</body>

</html>
