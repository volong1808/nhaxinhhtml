<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nhaxinh
 */

// get category slug in wordpress
if ( is_single() ) {
    $cats =  get_the_category();
    $cat = $cats[0];
} else {
    $cat = get_category( get_query_var( 'cat' ) );
}
$cate_slug = $cat->slug;
$cate_name = $cat->name;


$args = array(
    'posts_per_page' => 8,
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field' => 'slug',
            'terms' => $cate_slug
        )
    ),
    'post_status'=>'publish',
    'orderby' => 'post_date',
    'order' => 'DESC',
);
$cate_qr = new WP_Query($args);

get_header();
?>

<div class="post-popular">
    <h5><?php echo $cate_name ?> Được Xem Nhiều Nhất</h5>
    <div class="container-fluid">
        <div class="row">

            <?php
            if ($cate_qr->have_posts()) { ?>

                    <?php
                    while ($cate_qr->have_posts()) {
                        $cate_qr->the_post(); ?>

                        <div class="post-popular-item col-lg-3 col-md-6 col-sm-6 col-6">
                            <a class="entry-thumb" href="<?php the_permalink(); ?>">
                                <?php
                                if (has_post_thumbnail()) {
                                    echo get_the_post_thumbnail($cate_qr->post->ID, 'post-thumbnails', array( 'class' => 'w-100 h-auto' ));
                                } else { ?>
                                    <img class="w-100 h-auto" src="<?php echo get_template_directory_uri(); ?>/images/no-thumb.jpg" alt=""/>
                                    <?php
                                }
                                ?>
                            </a>
                            <a class="caption" href="<?php the_permalink(); ?>">
                                <?php echo $cate_qr->post->post_title; ?>
                            </a>
                        </div>
                        <?php
                    }
                    ?>
                <?php
            }
            ?>
        </div>
    </div>
</div>