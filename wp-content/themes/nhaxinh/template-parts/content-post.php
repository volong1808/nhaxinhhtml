<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nhaxinh
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <?php
        if (is_singular()) :
            the_title('<h1 class="entry-title">', '</h1>');
        else :
            the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
        endif;

        if ('post' === get_post_type()) :
            ?>
        <?php endif; ?>
    </header><!-- .entry-header -->
    <div class="post-meta">
                <span class="meta-cat">
                    <?php
                    foreach (get_the_category($post->ID) as $item) {
                        echo "<i class=\"mr-2 fa fa-book\"></i>";
                        echo '<a class="mr-2 text-uppercase" href="' .
                            get_category_link($item->cat_ID) .
                            '" title="' . $item->cat_name . '">' . $item->cat_name . '</a>';
                    }
                    ?>
                </span>
    </div>
    <?php nhaxinh_post_thumbnail(); ?>

    <div class="entry-content">
        <?php
        the_content(sprintf(
            wp_kses(
            /* translators: %s: Name of current post. Only visible to screen readers */
                __('Continue reading<span class="screen-reader-text"> "%s"</span>', 'nhaxinh'),
                array(
                    'span' => array(
                        'class' => array(),
                    ),
                )
            ),
            get_the_title()
        ));

        wp_link_pages(array(
            'before' => '<div class="page-links">' . esc_html__('Pages:', 'nhaxinh'),
            'after' => '</div>',
        ));
        ?>
    </div><!-- .entry-content -->
    <footer class="entry-footer text-center mb-3">
        <?php nhaxinh_entry_footer(); ?>
    </footer>
</article><!-- #post-<?php the_ID(); ?> -->
