<div class="infor-company text-center">
    <img class="m-auto" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" width="160" alt=""/>
    <p class="name-company text-uppercase"><?php echo get_option('company_name'); ?></p>
    <p class="phone-company">*HOTLINE : <?php echo get_option('hot_line1'); ?>*</p>
    <p>Email: <a href="mailto:<?php echo get_option('email'); ?>"><?php echo get_option('email'); ?></a></p>
    <p>
        Trụ Sở Chính: <br> <?php echo get_option('address'); ?>
        <br>
        Văn Phòng Kiến Trúc Xây Dựng: <br> <?php echo get_option('address_vp'); ?>
    </p>
</div>