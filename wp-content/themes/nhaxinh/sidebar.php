<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nhaxinh
 */
?>

<div class="sidebar">
    <section class="widget">
        <h3 class="widget-title"><span>TÌM KIẾM</span></h3>
        <div class="widget-container container">
            <form method="get" id="searchform" class="searchform" action="<?php echo home_url() ?>">
                <input class="form-control" type="text" name="s" id="searchinput">
                <input type="submit" id="searchsubmit">
            </form>
        </div>
    </section>
    <section class="widget">
        <h3 class="widget-title"><span>DANH MỤC</span></h3>
        <div class="widget-container container">
            <ul class="category">
                <?php
                $categories = get_categories();
                foreach ($categories as $category) {
                    if ($category->cat_ID != 1) { ?>
                        <li class="cate-item">
                            <a class="text-uppercase"
                               href="<?php echo get_category_link($category->cat_ID); ?>"><?php echo $category->name ?></a>
                        </li>
                    <?php }
                } ?>
            </ul>
        </div>
    </section>
    <?php $query = new WP_Query(array('pagename' => 'don-gia-tu-van'));
    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post(); ?>
            <section class="widget">
                <h3 class="widget-title"><span class="text-uppercase"><?php echo $query->post->post_title; ?></span></h3>
                <div class="widget-container container">
                    <a href="<?php the_permalink() ?>">
                        <?php
                        if (has_post_thumbnail()) {
                            echo get_the_post_thumbnail($query->post->ID, 'post-thumbnails',
                                array('class' => 'w-100 h-auto'));
                        } else { ?>
                            <img class="w-100 h-auto"
                                 src="<?php echo get_template_directory_uri(); ?>/images/don-gia.png" alt=""/>
                            <?php
                        }
                        ?>
                    </a>
                </div>
            </section>
            <?php
        }
    } else {
        ?>
        <section class="widget">
            <h3 class="widget-title"><span>ĐƠN GIÁ TƯ VẤN</span></h3>
            <div class="widget-container container">
                <img src="<?php echo get_template_directory_uri(); ?>/images/don-gia.png" alt="" class="w-100"/>
            </div>
        </section>
    <?php } ?>
</div>
