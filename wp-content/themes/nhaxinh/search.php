<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package nhaxinh
 */

get_header();
?>

    <div class="content">
        <div class="category-content">
            <h4 class="category-title">
                <span class="mr-1">Tìm kiếm cho: </span>
                <span><?php echo get_search_query(); ?></span>
            </h4>
            <div class="container-fluid">
                <div class="row">
                    <?php if (have_posts()) : ?>
                        <?php
                        /* Start the Loop */
                        while (have_posts()) :
                            the_post(); ?>
                            <div class="category-item col-lg-4 col-md-6 col-sm-12 col-12">
                                <a class="entry-thumb" href="<?php the_permalink(); ?>">
                                    <?php
                                    if (has_post_thumbnail()) {
                                        echo get_the_post_thumbnail($post->ID, 'post-thumbnails', array('class' => 'w-100 h-auto'));
                                    } else { ?>
                                        <img class="w-100 h-auto"
                                             src="<?php echo get_template_directory_uri(); ?>/images/no-thumb.jpg"
                                             alt=""/>
                                        <?php
                                    }
                                    ?>
                                </a>
                                <a class="caption" href="<?php the_permalink(); ?>">
                                    <?php echo $post->post_title; ?>
                                </a>
                            </div>
                        <?php
                        endwhile;

                    else :

                        echo "<p class='text-center'>Không có kết quả nào!</p>";

                    endif;
                    ?>
                </div>

            </div>
        </div>
    </div>

<?php
get_footer();
