<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package nhaxinh
 */

get_header();
?>

    <div class="content">
        <section class="post-content">
            <?php
            while ( have_posts() ) :
                the_post();

                get_template_part( 'template-parts/content', 'post' );

                get_template_part( 'template-parts/infor-company');
                get_template_part( 'template-parts/post-popular');

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;

            endwhile; // End of the loop.
            ?>
        </section>

        <?php get_sidebar(); ?>

        <div class="clearfix"></div>
    </div>

<?php
get_footer();