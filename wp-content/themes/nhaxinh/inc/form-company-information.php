<?php
/**
 * OngThep Information Form
 */
if (!empty($_POST['save-information'])) {
    $errorMessage = validateInput();
    if (empty($errorMessage)) {
        $company_name = $_POST['company_name'];
        $slogan = $_POST['slogan'];
        $description = $_POST['description'];
        $email = trim($_POST['email']);
        $phone = trim($_POST['phone']);
        $address = trim($_POST['address']);
        $address_vp = trim($_POST['address_vp']);
        $hot_line1 = trim($_POST['hot_line1']);
        $facebook_page = trim($_POST['facebook_page']);
        $twitter_page = trim($_POST['twitter_page']);
        $youtube_channel = trim($_POST['youtube_channel']);
        $youtube_about = trim($_POST['youtube_about']);
        $maps = trim($_POST['maps']);

        update_option('company_name', $company_name);
        update_option('slogan', $slogan);
        update_option('description', $description);
        update_option('email', $email);
        update_option('phone', $phone);
        update_option('address', $address);
        update_option('address_vp', $address_vp);
        update_option('hot_line1', $hot_line1);
        update_option('facebook_page', $facebook_page);
        update_option('twitter_page', $twitter_page);
        update_option('youtube_channel', $youtube_channel);
        update_option('youtube_about', $youtube_about);
        update_option('maps', $maps);

        $successMessage = "Cập Nhật Thành Công";
    }
}

$company_name = get_option('company_name');
$slogan = get_option('slogan');
$description = get_option('description');
$email = get_option('email');
$phone = get_option('phone');
$address = get_option('address');
$address_vp = get_option('address_vp');
$hot_line1 = get_option('hot_line1');
$facebook_page = get_option('facebook_page');
$twitter_page = get_option('twitter_page');
$youtube_channel = get_option('youtube_channel');
$youtube_about = get_option('youtube_about');
$maps = get_option('maps');

/**
 * Validate Nissin Information Forrm
 * @return string
 */
function validateInput() {
    $errorMessage = '';

    $company_name = $_POST['company_name'];
    $email = trim($_POST['email']);
    $phone = trim($_POST['phone']);
    $hot_line1 = trim($_POST['hot_line1']);
    $address = trim($_POST['address']);
    $facebook_page = trim($_POST['facebook_page']);
    $twitter_page = trim($_POST['twitter_page']);
    $youtube_channel = trim($_POST['youtube_channel']);
    $youtube_about = trim($_POST['youtube_about']);
    $maps = trim($_POST['maps']);

    if (empty($company_name)) {
        $errorMessage .= '<li>Tên Công Ty không được để trống</li>';
    }
    if (empty($email)) {
        $errorMessage .= '<li>Email không được để trống</li>';
    } else {
        if (!preg_match('/^[a-zA-Z][a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]+$/', $email)) {
            $errorMessage .= '<li>Email không đúng định dạng</li>';
        }
    }
    if (empty($phone)) {
        $errorMessage .= '<li>Số Điện Thoại không được để trống</li>';
    } else {
        if (!preg_match('/^\+{0,1}[\d+\s+\.+]{1,20}$/', $phone)) {
            $errorMessage .= '<li>Số Điện Thoại chỉ bao gồm số, dấu cách, dấu chấm và không dài hơn 20 ký tự';
        }
    }
    if (!preg_match('/^\+{0,1}[\d+\s+\.+]{1,20}$/', $hot_line1)) {
        $errorMessage .= '<li>Số Điện Thoại chỉ bao gồm số, dấu cách, dấu chấm và không dài hơn 20 ký tự';
    }

    if (empty($address)) {
        $errorMessage .= '<li>Địa chỉ không được để trống</li>';
    }
    if (!empty($facebook_page)) {
        if (!preg_match('/(^|\s)((https?:\/\/)?[\w-]+(\.[a-z-]+)+\.?(:\d+)?(\/\S*)?)/', $facebook_page)) {
            $errorMessage .= '<li>Địa chỉ Facebook không đúng</li>';
        }
    }
    if (!empty($twitter_page)) {
        if (!preg_match('/(^|\s)((https?:\/\/)?[\w-]+(\.[a-z-]+)+\.?(:\d+)?(\/\S*)?)/', $twitter_page)) {
            $errorMessage .= '<li>Địa chỉ Twitter không đúng</li>';
        }
    }

    if (!empty($youtube_channel)) {
        if (!preg_match('/(^|\s)((https?:\/\/)?[\w-]+(\.[a-z-]+)+\.?(:\d+)?(\/\S*)?)/', $youtube_channel)) {
            $errorMessage .= '<li>Địa chỉ Instagram không đúng</li>';
        }
    }

    if (!empty($youtube_about)) {
        if (!preg_match('/(^|\s)((https?:\/\/)?[\w-]+(\.[a-z-]+)+\.?(:\d+)?(\/\S*)?)/', $youtube_about)) {
            $errorMessage .= '<li>Địa chỉ youtube không đúng</li>';
        }
    }

    if (!empty($maps)) {
        if (!preg_match('/(^|\s)((https?:\/\/)?[\w-]+(\.[a-z-]+)+\.?(:\d+)?(\/\S*)?)/', $maps)) {
            $errorMessage .= '<li>Địa chỉ google maps không đúng</li>';
        }
    }

    if(!empty($errorMessage)) {
        $errorMessage = '<ul>' . $errorMessage . '</ul>';
    }
    return $errorMessage;
}
?>
<div class="wrap nosubsub">
    <h1 class="wp-heading-inline">Thông Tin Công Ty</h1>
    <hr class="wp-header-end">
    <div id="ajax-response">
        <?php
        if (!empty($errorMessage)) {
            ?>
            <div class="text-danger">
                <?php echo $errorMessage ?>
            </div>
        <?php } ?>
        <?php
        if (!empty($successMessage)) {
            ?>
            <div class="text-success">
                <?php echo $successMessage ?>
            </div>
        <?php } ?>
    </div>
    <div class="form-wrap row">
        <form method="post" id="nissin-information" action="" class="col-lg-6 col-sm-8 col-xs-12">
            <?php wp_nonce_field('update_options') ?>
            <div class="form-field">
                <label class="">Tên Công Ty<span class="text-danger">*</span></label>
                <input class="form-control" name="company_name" placeholder="Tên Công Ty" type="text" id="company_name" value="<?php echo $company_name ?>">
            </div>
            <div class="form-field">
                <label class="">Slogan Công Ty<span class="text-danger">*</span></label>
                <input class="form-control" name="slogan" placeholder="Slogan Công Ty" type="text" id="slogan" value="<?php echo $slogan ?>">
            </div>
            <div class="form-field">
                <label class="">Mô Tả Công Ty<span class="text-danger">*</span></label>
                <textarea class="form-control" name="description" id="description" placeholder="Mô Tả Công Ty"><?php echo $description ?></textarea>
            </div>
            <div class="form-field">
                <label class="">Email<span class="text-danger">*</span></label>
                <input class="form-control" name="email" placeholder="Email" type="text" id="email" value="<?php echo $email ?>">
            </div>
            <div class="form-field">
                <label class="">Số Điện Thoại<span class="text-danger">*</span></label>
                <input class="form-control" name="phone" placeholder="Số Điện Thoại" type="text" id="phone" value="<?php echo $phone ?>">
            </div>
            <div class="form-field">
                <label class="">Số Hot Line</label>
                <input class="form-control" name="hot_line1" placeholder="Số Hot Line1" type="text" id="hot_line1" value="<?php echo $hot_line1 ?>">
            </div>
            <div class="form-field">
                <label class="">Trụ Sở Chính<span class="text-danger">*</span></label>
                <input class="form-control" name="address" placeholder="Trụ Sở Chính" type="text" id="address" value="<?php echo $address ?>">
            </div>
            <div class="form-field">
                <label class="">Văn Phòng<span class="text-danger">*</span></label>
                <input class="form-control" name="address_vp" placeholder="Văn Phòng" type="text" id="address_vp" value="<?php echo $address_vp ?>">
            </div>
            <div class="form-field">
                <label class="">Facebook Fanpage</label>
                <input class="form-control" name="facebook_page" placeholder="Facebook Fanpage" type="text" id="facebook_page" value="<?php echo $facebook_page ?>">
            </div>
            <div class="form-field">
                <label class="">Twitter Fanpage</label>
                <input class="form-control" name="twitter_page" placeholder="Twitter Fanpage" type="text" id="twitter_page" value="<?php echo $twitter_page ?>">
            </div>
            <div class="form-field">
                <label class="">Youtube Channel</label>
                <input class="form-control" name="youtube_channel" placeholder="Youtube Channel" type="text" id="youtube_channel" value="<?php echo $youtube_channel ?>">
            </div>
            <div class="form-field">
                <label class="">Video Giới Thiệu</label>
                <input class="form-control" name="youtube_about" placeholder="Link Youtube" type="text" id="youtube_about" value="<?php echo $youtube_about ?>">
            </div>
            <div class="form-field">
                <label class="">Link Google Maps</label>
                <input class="form-control" name="maps" placeholder="Link Google Maps" type="text" id="maps" value="<?php echo $maps ?>">
            </div>
            <p class="submit">
                <input type="submit" class="button button-primary" name="save-information" value="Lưu Thay Đổi">
            </p>
        </form>
    </div>
</div>
