var nav_header = $("#navHeader");
var sticky = nav_header.offset().top;
$(window).scroll(function(){
    console.log(sticky);
    if ($(window).scrollTop() > sticky) {
        nav_header.addClass("sticky");
    } else {
        nav_header.removeClass("sticky");
    }
});

$(document).ready(function () {
    var htmlMenuHome = "   <li class=\"menu-item menu-home\">\n" +
        "      <a href=\"/\">\n" +
        "         <i class=\"fa fa-home\"></i>\n" +
        "      </a>\n" +
        "   </li>"
    $('.header-menu').prepend(htmlMenuHome);

    $('.sub-menu').prevAll().append("<i class='show-sub-menu ml-2 fa fa-chevron-down'></i>");

    var isMenu = false;
    $('#btn-menu').click(function () {
        if (isMenu === false) {
            isMenu = true;
        } else {
            isMenu = false;
        }
        console.log(isMenu);
        if (isMenu === true) {
            $('body').css('overflowY', 'hidden')
            $('.menu_sp').show();
            var clickSub = $('.menu_sp').find('.show-sub-menu').parent();
            clickSub.attr('href', 'javascript:void(0)');
            var isMenuSub = false;
            clickSub.on('click', function () {
                if (isMenuSub === false) {
                    isMenuSub = true;
                } else {
                    isMenuSub = false;
                }
                if (isMenuSub === true) {
                    $(this).parent().find('.sub-menu').slideDown();
                } else {
                    $(this).parent().find('.sub-menu').slideUp();
                }
            });
        } else {
            $('body').css("overflowY", 'auto');
            $('.menu_sp').hide();
        }
    });


    /*Scroll Top */
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('#scroll-top').fadeIn();
        } else {
            $('#scroll-top').fadeOut();
        }
    });

    $('#scroll-top').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 300);
        return false;
    });

});