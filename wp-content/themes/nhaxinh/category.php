<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nhaxinh
 */

// get category slug in wordpress
if ( is_single() ) {
    $cats =  get_the_category();
    $cat = $cats[0];
} else {
    $cat = get_category( get_query_var( 'cat' ) );
}
$cate_slug = $cat->slug;
$cate_name = $cat->name;

$args = array(
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field' => 'slug',
            'terms' => $cate_slug
        )
    ),
    'posts_per_page' => -1,
    'post_status' => 'publish',
    'orderby' => 'post_date',
    'order' => 'DESC',
);
$cate_qr = new WP_Query($args);

get_header();
?>
    <div class="content">
        <div class="category-content">
            <h4 class="category-title">
                <span class="mr-1">Thể loại</span>
                <a href="<?php get_category_link($cat->cat_ID); ?>" title=""><?php echo $cate_name; ?></a>
            </h4>
            <div class="container-fluid">
                <?php
                if ($cate_qr->have_posts()) { ?>
                    <div class="row">
                        <?php
                        while ($cate_qr->have_posts()) {
                            $cate_qr->the_post(); ?>
                            <div class="category-item col-lg-4 col-md-6 col-sm-12 col-12">
                                <a class="entry-thumb" href="<?php echo home_url($cate_slug . '/'. $cate_qr->post->post_name .'/' ); ?>">
                                    <?php
                                    if (has_post_thumbnail()) {
                                        echo get_the_post_thumbnail($cate_qr->post->ID, 'post-thumbnails', array( 'class' => 'w-100 h-auto' ));
                                    } else { ?>
                                        <img class="w-100 h-auto" src="<?php echo get_template_directory_uri(); ?>/images/no-thumb.jpg" alt=""/>
                                        <?php
                                    }
                                    ?>
                                </a>
                                <a class="caption" href="<?php echo home_url($cate_slug . '/'. $cate_qr->post->post_name .'/' ); ?>">
                                    <?php echo $cate_qr->post->post_title; ?>
                                </a>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                } else {
                    ?>
                    <p class="text-center">Không có bài viết nào</p>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>

<?php
get_footer();
