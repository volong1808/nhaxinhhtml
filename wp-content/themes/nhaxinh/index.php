<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nhaxinh
 */

get_header();
?>
    <div class="content">
        <section class="top-content">
            <h3 class="slogan-title text-center"><?php echo get_option('slogan'); ?></h3>
            <p>
                <span class="font-weight-bold"><?php echo get_option('company_name'); ?></span>
                <span class="font-italic">- <?php echo get_option('address_vp'); ?> - </span>
                <span>Hotline: </span><a href="tel:<?php echo get_option('phone'); ?>" class="font-weight-bold" style="color: #ce0100; "><?php echo get_option('phone'); ?></a>
            </p>
            <p class="slogan-description">
                <?php echo get_option('description'); ?>
            </p>
        </section>
        <?php
        $categories_slug = ['biet-thu', 'nha-pho', 'noi-that'];
        foreach ($categories_slug as $item_slug) {
            $cat = get_category_by_slug($item_slug);
            $cate_slug = $cat->slug;
            $cate_name = $cat->name;
            $args = array(
                'posts_per_page' => 30,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'category',
                        'field' => 'slug',
                        'terms' => $cate_slug
                    )
                ),
                'post_status' => 'publish',
                'orderby' => 'post_date',
                'order' => 'DESC',
            );
            $cate_qr = new WP_Query($args);
            ?>
            <section class="widget">
                <h3 class="widget-title text-uppercase"><span><?php echo $cate_name; ?></span><a
                            href="<?php echo home_url($cate_slug) ?>">NHẤP XEM NHIỀU
                        MẪU KHÁC</a></h3>
                <div class="widget-container container">
                    <div class="row">
                        <?php
                        if ($cate_qr->have_posts()) { ?>
                            <?php
                            while ($cate_qr->have_posts()) {
                                $cate_qr->the_post(); ?>
                                <div class="widget-item">
                                    <a class="entry-thumb" href="<?php echo home_url($cate_slug . '/'. $cate_qr->post->post_name .'/' ); ?>" style="opacity: 1; ">
                                        <?php
                                        if (has_post_thumbnail()) {
                                            echo get_the_post_thumbnail($cate_qr->post->ID, 'post-thumbnails', array('class' => 'w-100 h-auto'));
                                        } else { ?>
                                            <img class="w-100 h-auto"
                                                 src="<?php echo get_template_directory_uri(); ?>/images/no-thumb.jpg"
                                                 alt=""/>
                                            <?php
                                        }
                                        ?>
                                    </a>
                                    <a class="caption" href="<?php echo home_url($cate_slug . '/'. $cate_qr->post->post_name .'/' ); ?>" style="opacity: 1;">
                                        <?php echo $cate_qr->post->post_title; ?>
                                    </a>
                                </div>
                                <?php
                            }
                        } else {
                            ?>
                            <p class="text-center">Không có bài viết nào</p>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </section>
        <?php } ?>

        <section class="contact-section container">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <h3 class="contact-section__title">Liên Hệ Với Chúng Tôi</h3>
                    <div class="contact-section__description">
                        <p>Đối với yêu cầu tư vấn hoặc hỗ trợ kỹ thuật, xin vui lòng điền vào form bên dưới</p>
                    </div>
                    <?php echo do_shortcode( '[contact-form-7 id="1101" title="Contact form 1"]' ); ?>
                </div>
            </div>
        </section>
    </div>

<?php
get_footer();
