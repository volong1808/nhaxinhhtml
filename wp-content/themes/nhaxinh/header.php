<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nhaxinh
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php wp_title(); ?></title>

    <!-- Custom fonts for this theme -->
    <link href="<?php echo get_template_directory_uri()?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <link href="<?php echo get_template_directory_uri()?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <!-- Theme CSS -->
    <link href="<?php echo get_template_directory_uri()?>/css/main.css" rel="stylesheet">

</head>

<body <?php body_class(); ?>>
<div class="wrapper">
    <div class="top-container">
        <a href="/" class="text-center only_pc_sm">
            <img class="w-100" src="<?php echo get_template_directory_uri()?>/images/backgound-nha-xinh.png" alt=""/>
        </a>
        <a href="/" class="text-center only_sp_sm">
            <img class="" src="<?php echo get_template_directory_uri()?>/images/logo.png" width="100" alt=""/>
            <span style="line-height: 35px;">NHẤP VỀ TRANG CHỦ</span>
        </a>
    </div>
    <header id="navHeader">
        <nav class="navbar navbar-expand-lg navbar-dark menu">
            <a class="only_sp" href="<?php echo home_url(); ?>">Nhấn Xem Mẫu Nhà - Đơn Giá</a>
            <button id="btn-menu" class="navbar-toggler">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="menu_pc only_pc">
                <?php
                wp_nav_menu( array(
                    'theme_location' => 'menu-1',
                    'menu_class'     => 'header-menu',
                ) );?>
            </div>
            <div class="menu_sp" style="display: none;">
                <?php
                wp_nav_menu( array(
                    'theme_location' => 'menu-1',
                    'menu_class'     => 'header-menu',
                ) );?>
            </div>
        </nav>
    </header>

	<div id="content" class="site-content">
